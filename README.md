# README dcc_gpu_matlab_benchmark

This repository documents how to benchmark some MATLAB code on the GPU nodes.	

On your workstation:
```bash
ssh $USER@dcc-slogin-01.oit.duke.edu
```

On the head node:
```bash
module load Matlab/R2018b

WORKDIR=/work/$USER
mkdir $WORKDIR
cd $WORKDIR
scp -r $USER@gudenaa.egr.duke.edu:/getlab/nbb5/scratch/gpu_benchmark .
cd gpu_benchmark
mkdir output

# recompile the MEX code
srun --pty -p ultrasound-gpu bash
```

On the GPU node:
```bash
matlab
>> cd beamforming
>> compile_mex(1,1)
>> exit
exit
```

Back on the head node:
```bash
sbatch job.sh
```

Some sample results using the SLURM script included:
```
dcc-ultrasound-gpu-01
/opt/apps/rhel7/matlabR2018b/bin/matlab
Linux dcc-ultrasound-gpu-01 3.10.0-957.12.1.el7.x86_64 #1 SMP Wed Mar 20 11:34:37 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux
Tue Jul 16 13:52:27 EDT 2019

                            < M A T L A B (R) >
                  Copyright 1984-2018 The MathWorks, Inc.
                   R2018b (9.5.0.944444) 64-bit (glnxa64)
                              August 28, 2018


For online documentation, see https://www.mathworks.com/support
For product information, visit www.mathworks.com.

   CPU/GPU Beamformer   Time (s)
       CPU    Dyn Rcv      0.167
       GPU    Dyn Rcv      0.166
       CPU  Diverging      9.135
       GPU  Diverging      0.496
```
