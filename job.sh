#!/bin/bash
#SBATCH --partition=ultrasound-gpu
#SBATCH -o output/test%A%a.out # Standard output
#SBATCH -e output/test%A%a.err # Standard error
#SBATCH --cpus-per-task=8
#SBATCH --mem=24000
#SBATCH --mail-user=dpb6@duke.edu # Email to which notifications will be sent
#SBATCH --mail-type=END # Type of email notification- BEGIN,END,FAIL,ALL

uname -a
which matlab
date

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

cd /work/dpb6/gpu_benchmark

matlab -nodisplay -nosplash -nojvm -r 'benchmark(0,10); quit'

